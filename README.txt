////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pick-A-Kid formerly known as 'AdriLuna' a.k.a 'Numbers'
// Alejandro Ruiz Oct 2020
//
// Short description ///////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Tool to help pick a name among several, 'eenie meenie miney mo'-style
//
// Full description ////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Have you ever been confronted by kids that want to 'go first' or want to be the ones to make a decision for a group?
//
// Have you ever wished there was a truly random method to settle this that was also acceptable by all parties?
//
// This is Pick-A-Kid, formerly known as 'numberrrrrs'. It is a 'eenie meenie miney mo' type of decision process where each
// participant (up to six) gets to pick a number of their choice and then the app start counting up, starting with a random
// participant, towards the sum of all chosen numbers to see who gets selected in the end.
//
// You can enter the participant names and counting preferences (time between counting steps and maximum number to choose
// each participant), which then will be remembered within the device. It will also pick random numbers for each participant
// if you don't pick them yourself.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Finger pointing image source: https://www.kisscc0.com/clipart/index-finger-pointing-hand-pointer-pointer-zoxi7o/
// Logo background image source: https://www.pngguru.com/free-transparent-background-png-clipart-egncn
// Fireworks image source: https://www.hiclipart.com/free-transparent-background-png-clipart-iffgq/
// Speech bubble image source: https://www.nicepng.com/maxp/u2e6y3a9q8w7t4i1/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        Copyright 2020 Alejandro Ruiz
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Release log:
//
// 1.0 SVN 110: Release for Google Play Store open testing:
//              - Baseline functionality.
//
// 1.1 SVN 160: Release for Google Play Store production:
//              - Accepted tap outside keyboard to close it.
//              - New logo.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
