////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Pick-A-Kid formerly known as 'AdriLuna' a.k.a 'Numbers'
// Alejandro Ruiz Oct 2020
// Finger pointing image source: https://www.kisscc0.com/clipart/index-finger-pointing-hand-pointer-pointer-zoxi7o/
// Fireworks image source: https://www.hiclipart.com/free-transparent-background-png-clipart-iffgq/
// Speech bubble image source: https://www.nicepng.com/maxp/u2e6y3a9q8w7t4i1/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//        Copyright 2020 Alejandro Ruiz
//
//        Licensed under the Apache License, Version 2.0 (the "License");
//        you may not use this file except in compliance with the License.
//        You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
//        Unless required by applicable law or agreed to in writing, software
//        distributed under the License is distributed on an "AS IS" BASIS,
//        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//        See the License for the specific language governing permissions and
//        limitations under the License.
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

package com.alex.pickakid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;

public class MainActivity extends AppCompatActivity {

    // Preferences structure
    private SharedPreferences pref;

    // Preferences variables
    private int CountMs;
    private int MaxCount;
    private String Participants;

    // Preferences checking functions. Return -1 or null in case of a problem
    private int CheckCountTime(String CountTimeString) {
        if (CountTimeString == null) return -1;
        if (CountTimeString.isEmpty()) return -1;
        int TempCountTime = Integer.parseInt(CountTimeString);
        if ((TempCountTime < getResources().getInteger(R.integer.min_count_ms)) || (TempCountTime > getResources().getInteger(R.integer.max_count_ms)))
            return -1;
        return TempCountTime;
    }

    private int CheckMaxCount(String MaxCountString) {
        if (MaxCountString == null) return -1;
        if (MaxCountString.isEmpty()) return -1;
        int TempMaxCount = Integer.parseInt(MaxCountString);
        if ((TempMaxCount < getResources().getInteger(R.integer.min_max_count)) || (TempMaxCount > getResources().getInteger(R.integer.max_max_count)))
            return -1;
        return TempMaxCount;
    }

    private String CheckParticipants(String ParticipantsString) {
        if (ParticipantsString == null) return null;
        if (ParticipantsString.isEmpty()) return null;
        if ((ParticipantsString.split(",").length < getResources().getInteger(R.integer.min_participants)) ||
                (ParticipantsString.split(",").length > getResources().getInteger(R.integer.max_participants)))
            return null;
        return ParticipantsString;
    }

    private void readPreferences() {
        // Read preference file
        pref = getSharedPreferences("user_details", MODE_PRIVATE);
        // Check stored count time and use defaults if not correct
        CountMs = CheckCountTime(pref.getString("count_time_ms", null));
        if (CountMs == -1) {
            CountMs = getResources().getInteger(R.integer.default_count_ms);
        }
        // Check stored maximum count and use defaults if not correct
        MaxCount = CheckMaxCount(pref.getString("max_count_num", null));
        if (MaxCount == -1) {
            MaxCount = getResources().getInteger(R.integer.default_max_count);
        }
        // Check stored participants and use defaults if not correct
        Participants = CheckParticipants(pref.getString("participants", null));
        if (Participants == null) {
            Participants = getResources().getString(R.string.default_participants);
        }
    }

    public int applyPreferences(String StringNewCountMs, String StringNewMaxCount, String NewParticipants) {
        // Check the proposed count time
        int NewCountMs = CheckCountTime(StringNewCountMs);
        if (NewCountMs == -1) return 1;
        // Check the proposed maximum count
        int NewMaxCount = CheckMaxCount(StringNewMaxCount);
        if (NewMaxCount == -1) return 2;
        // Check the  proposed participants
        if (CheckParticipants(NewParticipants) == null) return 3;
        // Accept new values if the are all OK
        CountMs = NewCountMs;
        MaxCount = NewMaxCount;
        Participants = NewParticipants;
        // Store as new preferences
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("count_time_ms", String.valueOf(CountMs));
        editor.putString("max_count_num", String.valueOf(MaxCount));
        editor.putString("participants", Participants);
        editor.apply();
        return 0;
    }

    public int GetMaxCount() {
        return MaxCount;
    }

    public int GetCountMs() {
        return CountMs;
    }

    public String GetParticipants() {
        return Participants;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        // Hide soft keyboard if tapping anywhere in the fragment
        if (getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Read preferences
        readPreferences();
        // Set content
        setContentView(R.layout.activity_main);
    }

}