package com.alex.pickakid;

import androidx.annotation.NonNull;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class FragmentCount extends Fragment {

    // UI views used across functions
    private EditText[] chosen;
    private TextView[] current;
    private TextView sum;
    private ImageView finger;
    private ImageView speech;
    private ImageView fireworks;

    // Logic variables
    private final Random rand = new Random();
    private Timer autoUpdate;
    private int CurrentNumber;
    private int StartNumber;
    private int SumNumber;
    private int NumParticipants;
    private boolean Counting;

    // Local functions ///////////////////////////////////////////////////////////////////////////

    // doStep() - Execute the current step of the counting and pointing process
    private void doStep(int start_val, int current_val, int participants) {
        ConstraintLayout.LayoutParams params;
        int participant_now;

        // Calculate current participant being pointed at
        participant_now = (start_val + current_val) % participants;

        // Update current count value for all actual participants
        for (int i = 0; i < participants; i++) {
            if (i == participant_now) {
                current[i].setText(String.valueOf(current_val));
            } else {
                current[i].setText("");
            }
        }

        // Place finger pointing hand in the current participant
        params = (ConstraintLayout.LayoutParams) finger.getLayoutParams();
        if (participant_now == 0) {
            params.topToTop = params.bottomToBottom = R.id.tvName1;
        } else if (participant_now == 1) {
            params.topToTop = params.bottomToBottom = R.id.tvName2;
        } else if (participant_now == 2) {
            params.topToTop = params.bottomToBottom = R.id.tvName3;
        } else if (participant_now == 3) {
            params.topToTop = params.bottomToBottom = R.id.tvName4;
        } else if (participant_now == 4) {
            params.topToTop = params.bottomToBottom = R.id.tvName5;
        } else if (participant_now == 5) {
            params.topToTop = params.bottomToBottom = R.id.tvName6;
        }
        finger.requestLayout();
        finger.setVisibility(View.VISIBLE);

        // Place speech bubble in the current participant
        params = (ConstraintLayout.LayoutParams) speech.getLayoutParams();
        if (participant_now == 0) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent1;
        } else if (participant_now == 1) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent2;
        } else if (participant_now == 2) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent3;
        } else if (participant_now == 3) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent4;
        } else if (participant_now == 4) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent5;
        } else if (participant_now == 5) {
            params.topToTop = params.bottomToBottom = params.leftToLeft = params.rightToRight = R.id.tvCurrent6;
        }
        speech.requestLayout();
        speech.setVisibility(View.VISIBLE);
    }

    // abortCounting() - Stops any ongoing counting and pointing process and also clears up the user interface
    private void abortCounting() {
        // Stop a possible ongoing count
        if (Counting) {
            autoUpdate.cancel();
            ((Button) requireView().findViewById(R.id.bGo)).setText(getResources().getString(R.string.button_go_string));
            Counting = false;
        }
        // Clear current count values
        for (TextView textView : current) {
            textView.setText("");
        }
        // Clear total sum
        sum.setText("");
        // Clear all graphics
        fireworks.setVisibility(View.INVISIBLE);
        finger.setVisibility(View.INVISIBLE);
        speech.setVisibility(View.INVISIBLE);
        // Clear chosen numbers to be safe - they might no longer be valid after a preferences update
        for (EditText editText : chosen) {
            editText.setText("");
        }
    }

    // calculateNumbers() - Initiates a counting and pointing process
    private void calculateNumbers() {
        // State check - if we are already in a counting and pointing process we will instead about it
        if (Counting) {
            abortCounting();
            return;
        }

        // Only need to clear the fireworks for a new counting and pointing process
        // The finger and speech can stay since we'll restart immediately
        fireworks.setVisibility(View.INVISIBLE);

        // Start counting state, now button becomes abort
        Counting = true;
        ((Button) requireView().findViewById(R.id.bGo)).setText(getResources().getString(R.string.button_abort_string));

        // If the chosen numbers are not pre-filled or correct, populate them randomly
        for (EditText editText : chosen) {
            if ((editText.getText().toString().isEmpty()) ||
                    (Integer.parseInt(editText.getText().toString()) < getResources().getInteger(R.integer.min_max_count)) ||
                    (Integer.parseInt(editText.getText().toString()) > ((MainActivity) requireActivity()).GetMaxCount())) {
                editText.setText(String.valueOf(rand.nextInt(((MainActivity) requireActivity()).GetMaxCount()) + 1));
            }
        }

        // Add the numbers from each actual participant and show the sum on the UI
        SumNumber = 0;
        for (int i = 0; i < NumParticipants; i++) {
            SumNumber += Integer.parseInt(chosen[i].getText().toString());
        }
        sum.setText(String.format(getResources().getString(R.string.equal_sum), SumNumber));

        // Start randomly from any of the participants
        StartNumber = rand.nextInt(NumParticipants);

        // Start counting up with a timer
        CurrentNumber = 1;
        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                requireActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        // One step at a time
                        doStep(StartNumber, CurrentNumber, NumParticipants);
                        // Check for process end when we reach the total sum
                        if (CurrentNumber == SumNumber) {
                            // Stop counting timer, button becomes go again
                            autoUpdate.cancel();
                            Counting = false;
                            ((Button) requireView().findViewById(R.id.bGo)).setText(getResources().getString(R.string.button_go_string));
                            // Fireworks placement
                            ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) fireworks.getLayoutParams();
                            int winner = (StartNumber + CurrentNumber) % NumParticipants;
                            if (winner == 0) {
                                params.topToTop = params.bottomToBottom = R.id.tvName1;
                            } else if (winner == 1) {
                                params.topToTop = params.bottomToBottom = R.id.tvName2;
                            } else if (winner == 2) {
                                params.topToTop = params.bottomToBottom = R.id.tvName3;
                            } else if (winner == 3) {
                                params.topToTop = params.bottomToBottom = R.id.tvName4;
                            } else if (winner == 4) {
                                params.topToTop = params.bottomToBottom = R.id.tvName5;
                            } else if (winner == 5) {
                                params.topToTop = params.bottomToBottom = R.id.tvName6;
                            }
                            fireworks.setVisibility(View.VISIBLE);
                            fireworks.requestLayout();
                        } else {
                            CurrentNumber++;                    // Increase count
                        }
                    }
                });
            }
        }, 0, ((MainActivity) requireActivity()).GetCountMs()); // updates every half second sec
    }

    // Class methods //////////////////////////////////////////////////////////////////////////////

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Needs toolbar options for navigation to preferences fragment
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_count, container, false);
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        // Inflate the toolbar options to include navigation to preferences button
        inflater.inflate(R.menu.toolbar_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set the Toolbar to act as the ActionBar for this fragment
        Toolbar toolbar = view.findViewById(R.id.toolbar_count);
        ((AppCompatActivity) requireActivity()).setSupportActionBar(toolbar);
        // Toolbar is clickable for app about toast pop-up
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast toast = Toast.makeText(requireActivity().getApplicationContext(),
                        String.format(
                                getResources().getString(R.string.app_about),
                                getResources().getString(R.string.app_name),
                                getResources().getString(R.string.app_version),
                                getResources().getString(R.string.app_URL)),
                        Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();
            }
        });

        // Find views that we need to access
        chosen = new EditText[]{
                view.findViewById(R.id.etCount1),
                view.findViewById(R.id.etCount2),
                view.findViewById(R.id.etCount3),
                view.findViewById(R.id.etCount4),
                view.findViewById(R.id.etCount5),
                view.findViewById(R.id.etCount6)
        };
        TextView[] names = new TextView[]{
                view.findViewById(R.id.tvName1),
                view.findViewById(R.id.tvName2),
                view.findViewById(R.id.tvName3),
                view.findViewById(R.id.tvName4),
                view.findViewById(R.id.tvName5),
                view.findViewById(R.id.tvName6)
        };
        current = new TextView[]{
                view.findViewById(R.id.tvCurrent1),
                view.findViewById(R.id.tvCurrent2),
                view.findViewById(R.id.tvCurrent3),
                view.findViewById(R.id.tvCurrent4),
                view.findViewById(R.id.tvCurrent5),
                view.findViewById(R.id.tvCurrent6)
        };
        sum = view.findViewById(R.id.tvTotalSum);
        finger = view.findViewById(R.id.ivPointer);
        speech = view.findViewById(R.id.ivSpeech);
        fireworks = view.findViewById(R.id.ivFireworks);

        // Runtime update of participants and title
        ((TextView) view.findViewById(R.id.tvNumbers)).setText(String.format(
                getResources().getString(R.string.numbers_title),
                getResources().getInteger(R.integer.min_max_count),
                ((MainActivity) requireActivity()).GetMaxCount()));

        // Runtime update of participants names and visibility
        NumParticipants = ((MainActivity) requireActivity()).GetParticipants().split(",").length;
        for (int i = 0; i < NumParticipants; i++) {
            names[i].setText(((MainActivity) requireActivity()).GetParticipants().split(",")[i]);
            names[i].setVisibility(View.VISIBLE);
            chosen[i].setVisibility(View.VISIBLE);
        }

        // Move total sum and buttons up if we are not using them all
        ConstraintLayout.LayoutParams params;
        switch (NumParticipants) {
            case 2:
                params = (ConstraintLayout.LayoutParams) sum.getLayoutParams();
                params.topToBottom = R.id.etCount2;                         // Total sum connects to etCount2
                sum.requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[1].getLayoutParams();
                params.bottomToTop = R.id.tvTotalSum;                       // etCount2 connects to total sum
                chosen[1].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[2].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount3 is not visible but needs to be looped somewhere
                chosen[2].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[3].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount4 is not visible but needs to be looped somewhere
                chosen[3].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[4].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount5 is not visible but needs to be looped somewhere
                chosen[4].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[5].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount6 is not visible but needs to be looped somewhere
                chosen[5].requestLayout();
                break;
            case 3:
                params = (ConstraintLayout.LayoutParams) sum.getLayoutParams();
                params.topToBottom = R.id.etCount3;                         // Total sum connects to etCount3
                sum.requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[2].getLayoutParams();
                params.bottomToTop = R.id.tvTotalSum;                       // etCount3 connects to total sum
                chosen[2].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[3].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount4 is not visible but needs to be looped somewhere
                chosen[3].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[4].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount5 is not visible but needs to be looped somewhere
                chosen[4].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[5].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount6 is not visible but needs to be looped somewhere
                chosen[5].requestLayout();
                break;
            case 4:
                params = (ConstraintLayout.LayoutParams) sum.getLayoutParams();
                params.topToBottom = R.id.etCount4;                         // Total sum connects to etCount4
                sum.requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[3].getLayoutParams();
                params.bottomToTop = R.id.tvTotalSum;                       // etCount4 connects to total sum
                chosen[3].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[4].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount5 is not visible but needs to be looped somewhere
                chosen[4].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[5].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount6 is not visible but needs to be looped somewhere
                chosen[5].requestLayout();
                break;
            case 5:
                params = (ConstraintLayout.LayoutParams) sum.getLayoutParams();
                params.topToBottom = R.id.etCount5;                         // Total sum connects to etCount5
                sum.requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[4].getLayoutParams();
                params.bottomToTop = R.id.tvTotalSum;                       // etCount5 connects to total sum
                chosen[4].requestLayout();
                params = (ConstraintLayout.LayoutParams) chosen[5].getLayoutParams();
                params.bottomToTop = params.topToBottom = R.id.tvTotalSum;  // etCount6 is not visible but needs to be looped somewhere
                chosen[5].requestLayout();
                break;
            default:
                break;
        }

        // 'Go' button click listener
        view.findViewById(R.id.bGo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calculateNumbers();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_preferences) {
            // Stop any ongoing count
            abortCounting();
            // Navigate to preferences fragment
            NavHostFragment.findNavController(FragmentCount.this)
                    .navigate(R.id.action_CountFragment_to_SettingsFragment);
            return true;
        }
        // If we got here, the user's action was not recognized, invoke the superclass to handle it
        return super.onOptionsItemSelected(item);
    }
}