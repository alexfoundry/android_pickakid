package com.alex.pickakid;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class FragmentSettings extends Fragment {

    // UI views used across functions
    private EditText setCountMs;
    private EditText setMaxCount;
    private EditText setParticipants;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Needs toolbar options for navigation back button
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        // Inflate the toolbar options to include navigation back button
        inflater.inflate(R.menu.toolbar_preferences, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_back) {
            // Validate preferences by trying to apply them and looking at the returned value
            switch (((MainActivity) requireActivity()).applyPreferences(setCountMs.getText().toString(),
                    setMaxCount.getText().toString(),
                    setParticipants.getText().toString())) {
                case 1: // Incorrect Count time
                    setCountMs.setText(String.valueOf(((MainActivity) requireActivity()).GetCountMs()));
                    break;
                case 2: // Incorrect Maximum count
                    setMaxCount.setText(String.valueOf(((MainActivity) requireActivity()).GetMaxCount()));
                    break;
                case 3: // Incorrect Participants
                    setParticipants.setText(((MainActivity) requireActivity()).GetParticipants());
                    break;
                default: // Everything OK
                    NavHostFragment.findNavController(FragmentSettings.this).navigateUp();
                    return true;
            }
        }
        // If we got here, the user's action was not recognized, invoke the superclass to handle it
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set the Toolbar to act as the ActionBar for this fragment
        Toolbar toolbar = view.findViewById(R.id.toolbar_prefs);
        ((AppCompatActivity) requireActivity()).setSupportActionBar(toolbar);

        // Remove the app title from the Toolbar on this preferences fragment
        ActionBar actionbar = ((AppCompatActivity) requireActivity()).getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
        }

        // Fill in the UI editable fields with the current preference values
        setCountMs = view.findViewById(R.id.etSettingCountMs);
        setMaxCount = view.findViewById(R.id.etSettingMaxCount);
        setParticipants = view.findViewById(R.id.etParticipants);

        setCountMs.setText(String.valueOf(((MainActivity) requireActivity()).GetCountMs()));
        setMaxCount.setText(String.valueOf(((MainActivity) requireActivity()).GetMaxCount()));
        setParticipants.setText(((MainActivity) requireActivity()).GetParticipants());

        // Fill in min and max values in the preference titles
        ((TextView) view.findViewById(R.id.tvCountMs)).setText(String.format(
                getResources().getString(R.string.count_ms_setting),
                getResources().getInteger(R.integer.min_count_ms),
                getResources().getInteger(R.integer.max_count_ms)));

        ((TextView) view.findViewById(R.id.tvMaxCount)).setText(String.format(
                getResources().getString(R.string.max_count_setting),
                getResources().getInteger(R.integer.min_max_count),
                getResources().getInteger(R.integer.max_max_count)));

        ((TextView) view.findViewById(R.id.tvParticipants)).setText(String.format(
                getResources().getString(R.string.participants_setting),
                getResources().getInteger(R.integer.min_participants),
                getResources().getInteger(R.integer.max_participants)));
    }
}